﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Prog3Beadando 
{
    public class ViewModel : INotifyPropertyChanged
    {
        ObservableCollection<Palya> palyak;
        int palyakSzama;
        int palyaNum = 0;
        Jatekos plyr;        
                
        public Jatekos Plyr { get { return plyr; } set { plyr = value; OPC(); } }
        public ObservableCollection<Palya> Palyak { get { return palyak; } set { palyak = value; } }
        

        public event PropertyChangedEventHandler PropertyChanged;

        private void OPC([CallerMemberName]string n = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(n));
        }


        public ViewModel()
        {
            plyr = new Jatekos("Unknown Player");
            palyaNum++;
            palyaNum--;
            palyakSzama = 5;

            palyak = new ObservableCollection<Palya>();

            
            for (int i = 1; i < palyakSzama + 1; i++)
            {
                palyak.Add(PalyaManager.Beolvas(i));
            }

            EredmenyManager.EredmenyBeolvas(palyak);
            EredmenyManager.Rendez(palyak);
        }       

    }

}
