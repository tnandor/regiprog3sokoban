﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Prog3Beadando
{
    /// <summary>
    /// Interaction logic for EredmenyWindow.xaml
    /// </summary>
    public partial class EredmenyWindow : Window
    {
        ViewModel vm;

        public EredmenyWindow(ViewModel be)
        {
            vm = be;
            DataContext = vm;
            InitializeComponent();
        }

        /// <summary>
        /// TextBox feltöltése a kiválasztott pálya eredményeivel
        /// </summary>
        private void button_Click(object sender, RoutedEventArgs e)
        {
            EredmenyManager.EredmenyBeolvas(vm.Palyak);            
            textBox.Text = "";
            foreach (Eredmeny ered in (comboBox.SelectedItem as Palya).Eredmenyek)
            {
                textBox.Text += ered.Nev + "                   " + ered.LepesSzam + "\n";
            }
        }

    }


}



