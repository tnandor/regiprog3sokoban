﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Prog3Beadando
{

    public partial class GameWindow : Window
    {

        Palya dc;
        private Jatekos a;
        int cntofLepes;
        int x = 0;
        public event EventHandler UjraKezd;

        /// <summary>
        /// A példány játékosának külső eléréséhez tulajdoság
        /// </summary>
        public Jatekos A { get { return a; } set { a = value; } }

        public GameWindow(Palya be, Jatekos j)
        {
            if (UjraKezd!=null)
            {
                string y = UjraKezd.ToString();
                y += "";               
            }
            
            a = j;
            InitializeComponent();
            dc = be;
            DataContext = dc;
            cntofLepes = 0;
            KeyDown += GameWindow_KeyDown;
        }


        /// <summary>
        /// Az irányítás kezeléséért felelős metódus
        /// A KeyDown eventre a megfelelő értékkel mozgatja a karaktert a lenyomott nyílnak megfelelően
        /// R a pálya újrakezdését teszi lehetővé
        /// </summary>
        private void GameWindow_KeyDown(object sender, KeyEventArgs e)
        {
            PalyaManager.CelbaErt += PalyaManager_CelbaErt;
            UjraKezd += PalyaManager_UjraKezd;

            switch (e.Key)
            {
                case Key.Left: PalyaManager.Mozog(dc, -1, 0); cntofLepes++; break;
                case Key.Right: PalyaManager.Mozog(dc, 1, 0); cntofLepes++; break;
                case Key.Up: PalyaManager.Mozog(dc, 0, -1); cntofLepes++; break;
                case Key.Down: PalyaManager.Mozog(dc, 0, 1); cntofLepes++; break;
                case Key.R: PalyaManager_UjraKezd(null, null); cntofLepes = 0; break;
                //case Key.Q: PalyaManager_CelbaErt(null, null); break;
            }

        }

        /// <summary>
        /// A pálya alaphelyzetbe állításáért felelős metódus
        /// </summary>
        private void PalyaManager_UjraKezd(object sender, EventArgs e)
        {
            Palya temp = PalyaManager.Beolvas(dc.PalyaNum);
            dc.Dobozok = temp.Dobozok;
            dc.Jatekos = temp.Jatekos;
        }

        /// <summary>
        /// Ha minden lerakóhelyen van doboz a pálya késznek tekinthető az alábbi metódus gratulál és bezárja a játék ablakot és továbbítja a lépésszámot.        
        /// </summary>
        private void PalyaManager_CelbaErt(object sender, EventArgs e)
        {
            x++;
            if (x == 1)
            {
                MessageBox.Show("Gratulálok!\nTeljesítetted a pályát " + cntofLepes + " lépésben");
                a.Pontszam = cntofLepes.ToString();
                DialogResult = true;
            }
        }

        
    }
}
