﻿namespace Prog3Beadando
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class Jatekos
    {
        /// <summary>
        /// A tulajdonság biztosítja a név külső olvasását
        /// </summary>
        public string Nev { get; }

        /// <summary>
        /// A tulajdonság biztosítja a Pontszám külső olvasását és írását
        /// </summary>
        public string Pontszam { get; set; }

        
        public Jatekos(string nev)
        {
            Nev = nev;
        }

        /// <summary>
        /// ToString() felülírása, hogy a játékos neve jelenjen meg a MainWindowon
        /// </summary>
        public override string ToString()
        {
            return Nev;
        }
    }

 

}
