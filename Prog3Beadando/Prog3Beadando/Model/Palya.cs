﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Prog3Beadando
{
    public class Palya : INotifyPropertyChanged
    {
        public const int Mezo = 40;

        bool[,] falak;
        Point[] lerakok;
        Point[] dobozok;
        Point jatekos;
        int palyaNum;
        List<Eredmeny> eredmenyek;

        
        public event PropertyChangedEventHandler PropertyChanged;

        private void OPC([CallerMemberName]string n = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(n));
        }


        /// <summary>
        /// A példány eredményeinek külső eléréséhez tulajdonság 
        /// </summary>
        public List<Eredmeny> Eredmenyek { get { return eredmenyek; } set { eredmenyek = value; } }

        /// <summary>
        /// A példány nevének külső eléréséhez tulajdonság 
        /// </summary>
        public string Nev { get; set; }

        /// <summary>
        /// A példány sorszámának külső eléréséhez tulajdonság 
        /// </summary>
        public int PalyaNum { get { return palyaNum; } set { palyaNum = value; } }

        /// <summary>
        /// A példány falainak külső eléréséhez tulajdonság 
        /// </summary>
        public bool[,] Falak
        {
            get { return falak; }
            set { falak = value; }
        }

        /// <summary>
        /// A példányhoz tartozó játékos külső eléréséhez tulajdonság 
        /// Módosítása esetén elsüti a propertychanged eseményt, hogy mozgatása esetén változzon a megjelenítés
        /// </summary>
        public Point Jatekos
        {
            get { return jatekos; }
            set { jatekos = value; OPC(); }
        }

        /// <summary>
        /// A példány lerakó helyeinek külső eléréséhez tulajdonság 
        /// </summary>
        public Point[] Lerakok
        {
            get { return lerakok; }
            set { lerakok = value; }
        }

        /// <summary>
        /// A példány dobozainak külső eléréséhez tulajdonság 
        /// Módosítása esetén elsüti a propertychanged eseményt, hogy mozgatása esetén változzon a megjelenítés
        /// </summary>
        public Point[] Dobozok
        {
            get { return dobozok; }
            set { dobozok = value; OPC(); }
        }

        /// <summary>
        /// A falhoz tartozó kép betöltésére alkalmas
        /// </summary>
        public ImageBrush FalBrush
        {
            get { return PalyaManager.GetImageBrush("_fal.bmp"); }
        }

        /// <summary>
        /// A dobozhoz tartozó kép betöltésére alkalmas
        /// </summary>
        public ImageBrush DobozBrush
        {
            get { return PalyaManager.GetImageBrush("_doboz.bmp"); }
        }

        /// <summary>
        /// A játekoshoz tartozó kép betöltésére alkalmas
        /// </summary>
        public ImageBrush JatekosBrush
        {
            get { return PalyaManager.GetImageBrush("_Karakter.bmp"); }
        }

        /// <summary>
        /// A lerkóhelyhez tartozó kép betöltésére alkalmas
        /// </summary>
        public ImageBrush LerakoBrush
        {
            get { return PalyaManager.GetImageBrush("_exit.bmp"); }
        }

        /// <summary>
        /// ToString() felülírása, hogy a MainWindow listában a nevük látszódjon.
        /// </summary>
        public override string ToString()
        {
            return Nev;
        }

    }

}


