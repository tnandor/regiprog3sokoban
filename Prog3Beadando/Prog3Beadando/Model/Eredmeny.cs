﻿//komment
namespace Prog3Beadando
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class Eredmeny : IComparable<Eredmeny>
    {
        public string Nev { get; set; }
        public string LepesSzam { get; set; }


        /// <summary>
        /// Egy-egy pályához tartozó eredmények a VS nyújtotta List<T> típusban tárolok
        /// ahhoz, hogy rendezhessem azt lépésszámok szerint az Eredménynek meg kell valósítania az Icomparable Interface-t
        /// Ehhez kellett a CompareTo
        /// </summary>
        public int CompareTo(Eredmeny other)
        {
            if (other != null)
            {
                return int.Parse(LepesSzam).CompareTo(int.Parse(other.LepesSzam));
            }
            else
            {
                throw new ArgumentException("Az objektum nem eredmény");
            }

        }

        /// <summary>
        /// Icomparable interface miatt felülírás
        /// </summary>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            Eredmeny other = (Eredmeny)obj;
            return this.LepesSzam == other.LepesSzam;
        }

        /// <summary>
        /// Icomparable interface miatt felülírás
        /// </summary>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Icomparable interface miatt felülírás
        /// </summary>
        public static bool operator !=(Eredmeny e1, Eredmeny e2)
        {
            if ((object)e1 == null)
            {
                return (object)e2 == null;
            }

            return !e1.Equals(e2);
        }

        /// <summary>
        /// Icomparable interface miatt felülírás
        /// </summary>
        public static bool operator ==(Eredmeny e1, Eredmeny e2)
        {
            if ((object)e1 == null)
            {
                return (object)e2 == null;
            }

            return e1.Equals(e2);         
         }

        /// <summary>
        /// Icomparable interface miatt felülírás
        /// </summary>
        public static bool operator <(Eredmeny e1, Eredmeny e2)
        {
            return e1.CompareTo(e2) == -1;
        }

        /// <summary>
        /// Icomparable interface miatt felülírás
        /// </summary>
        public static bool operator >(Eredmeny e1, Eredmeny e2)
        {
            return e1.CompareTo(e2) == 1;
        }

    }
}
