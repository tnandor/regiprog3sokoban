﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog3Beadando
{
    public static class EredmenyManager
    {
        /// <summary>
        /// Fájból beolvassa az pályákhoz tartozó eredményeket majd rendezi azt.         
        /// </summary>
        public static void EredmenyBeolvas(ObservableCollection<Palya> p)
        {
            foreach (Palya paly in p)
            {
                paly.Eredmenyek = new List<Eredmeny>();
            }

            string szoveg = "";

            szoveg = File.ReadAllText("Eredmenyek.scores");

            string[] egypalya = szoveg.Split('#');
            int palyaCnt = egypalya.Length;

            for (int i = 0; i < palyaCnt; i++)
            {
                string[] emberek = egypalya[i].Split(',');

                for (int j = 0; j < emberek.Length; j++)
                {
                    if (emberek[j].Split('@')[0] != "")
                    {
                        string[] adatok = emberek[j].Split('@');
                        p[i].Eredmenyek.Add(new Eredmeny() { Nev = adatok[0], LepesSzam = adatok[1] });
                    }


                }

            }
            EredmenyManager.Rendez(p);

        }

        /// <summary>
        /// Fájlba írja a pályákhhoz tartozó eredményeket rendezve.
        /// </summary>
        public static void EredmenyKiir(ObservableCollection<Palya> p)
        {
            EredmenyManager.Rendez(p);

            string ki = "";

            for (int j = 0; j < p.Count; j++)
            {
                for (int i = 0; i < p[j].Eredmenyek.Count; i++)
                {
                    ki += (p[j].Eredmenyek[i].Nev + '@' + p[j].Eredmenyek[i].LepesSzam);
                    if (i < p[j].Eredmenyek.Count - 1)
                    {
                        ki += (",");
                    }
                }
                if (j < p.Count - 1)
                {
                    ki += ('#');
                }
            }
            File.WriteAllText("Eredmenyek.scores", ki);
        }

        /// <summary>
        /// Eredmények rendezéséért felelős metódus
        /// </summary>
        public static void Rendez(ObservableCollection<Palya> p)
        {

            foreach (Palya paly in p)
            {
                try
                {
                    paly.Eredmenyek.Sort();
                }
                catch (NullReferenceException)
                {

                }

            }
        }

    }

}

