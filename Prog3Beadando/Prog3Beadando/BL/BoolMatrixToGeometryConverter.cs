﻿/// <header>
/// Bool mátrixból a későbbiekben megjelenítésre alkalmas Geometryt előállítő osztály
/// </header>
namespace Prog3Beadando
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;
    using System.Windows.Media;
    
    class BoolMatrixToGeometryConverter : IValueConverter
    {
        /// <summary>
        /// A convertálást megvalósító metódus
        /// </summary>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {           
            bool[,] m = (bool[,])value;
            GeometryGroup gg = new GeometryGroup();
            for (int i = 0; i < m.GetLength(0); i++)
            {
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    if (m[i, j])
                    {
                        gg.Children.Add(new RectangleGeometry(new System.Windows.Rect(j * Palya.Mezo, i * Palya.Mezo, Palya.Mezo, Palya.Mezo)));
                    }                        
                }
            }
            return gg;
        }

        /// <summary>
        /// interfacehez szükséges metódus
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {            
            throw new NotImplementedException();
        }
    }
}
