﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Prog3Beadando
{
    
    class PointArrayToGeometryConverter : IValueConverter
    {
        /// <summary>
        /// Point tömböt geometryvé alakító metódus
        /// </summary>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Point[] p = (Point[])value;
            GeometryGroup gg = new GeometryGroup();

            for (int i = 0; i < p.Length; i++)
            {
                gg.Children.Add(new RectangleGeometry(new Rect(p[i].X * Palya.Mezo, p[i].Y * Palya.Mezo, Palya.Mezo, Palya.Mezo)));
            }

            return gg;
        }

        /// <summary>
        /// Geometryt Point tömbbe visszaalakító metódus de nem implementáltam mert nincs szükség rá
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
