﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Prog3Beadando
{
    class PointToGeometryConverter : IValueConverter
    {
        /// <summary>
        /// Point-ot Geometry-vé alakító metódus
        /// </summary>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Point p = (Point)value;
            return new RectangleGeometry(new Rect(p.X * Palya.Mezo, p.Y * Palya.Mezo, Palya.Mezo, Palya.Mezo));
        }

        /// <summary>
        /// Geometryt Point-á visszaalakító metódus lenne de nem implementáltam szükség híjján.
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
