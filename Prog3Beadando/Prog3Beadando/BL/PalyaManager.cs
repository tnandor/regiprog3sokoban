﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Prog3Beadando
{
    public static class PalyaManager
    {


        /// <summary>
        /// Palyak fajlbol történő beolvasásáért felelős metódus
        /// </summary>
        /// <param name="palyanum">palyanum a palya sorszama</param>
        public static Palya Beolvas(int palyanum)
        {
            Palya p = new Palya();
            p.PalyaNum = palyanum;
            string[] lines = System.IO.File.ReadAllLines("L0" + palyanum + ".lvl");
            p.Nev = "L0" + palyanum;
            p.Falak = new bool[int.Parse(lines[1]), int.Parse(lines[0])];
            int cntOfItems = int.Parse(lines[2]);
            p.Lerakok = new Point[cntOfItems];
            p.Dobozok = new Point[cntOfItems];

            int indexOfLerakok = cntOfItems - 1;
            int indexOfDobozok = cntOfItems - 1;

            for (int i = 3; i < lines.Length; i++)
            {
                for (int j = 0; j < lines[i].Length; j++)
                {
                    if (lines[i][j] == 'X')
                    {
                        p.Falak[i - 3, j] = true;
                    }
                    else if (lines[i][j] == '@')
                    {
                        p.Jatekos = new Point(j, i - 3);
                    }
                    else if (lines[i][j] == '.')
                    {
                        p.Lerakok[indexOfLerakok] = new Point(j, i - 3);
                        indexOfLerakok--;
                    }
                    else if (lines[i][j] == '*')
                    {
                        p.Dobozok[indexOfDobozok] = new Point(j, i - 3);
                        indexOfDobozok--;
                    }
                }

            }

            return p;
        }


        /// <summary>
        /// Kép fájból történő beolvasásért felelős metódus
        /// Ez felel a játék grafikáiért.
        /// </summary>
        public static ImageBrush GetImageBrush(string path)
        {
            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(path, UriKind.Relative)));
            ib.TileMode = TileMode.Tile;
            ib.Viewport = new Rect(0, 0, Palya.Mezo, Palya.Mezo);
            ib.ViewportUnits = BrushMappingMode.Absolute;
            return ib;
        }

        public static event EventHandler CelbaErt;




        /// <summary>
        /// A karaktert mozgatja a p-pályán
        /// dx paraméter adja meg a szélessi mozgást
        /// dy pedig a magasságit
        /// </summary>
        public static void Mozog(Palya p, int dx, int dy)
        {

            ///Jatekos mozgatásra falba ütközik-e, valós lépés-e
            
            if (p.Jatekos.Y + dy >= p.Falak.GetLength(0) ||
               p.Jatekos.Y + dy < 0 ||
              p.Jatekos.X + dx >= p.Falak.GetLength(1) ||
              p.Jatekos.X + dx < 0 || p.Falak[(int)p.Jatekos.Y + dy, (int)p.Jatekos.X + dx])
                return;

            for (int i = 0; i < p.Dobozok.Length; i++)
            {

                /// Jatekos kovetkezo poziciója doboz
                if (p.Jatekos.X + dx == p.Dobozok[i].X && p.Jatekos.Y + dy == p.Dobozok[i].Y)
                {
                    ///doboz kovetkezo pozíciója fal
                    if (p.Falak[(int)p.Dobozok[i].Y + dy, (int)p.Dobozok[i].X + dx])
                    {
                        return;
                    }

                    for (int j = 0; j < p.Dobozok.Length; j++)
                    {
                        ///doboz következő pozíciója doboz
                        if (p.Dobozok[i].X + dx == p.Dobozok[j].X && p.Dobozok[i].Y + dy == p.Dobozok[j].Y)
                        {
                            return;
                        }
                    }
                    ///doboz mozgatása érvényes -- doboz pálya határain belül, doboz következő pozíciója nem fal
                    if (!((p.Dobozok[i].Y + dy > p.Falak.GetLength(0) || (p.Dobozok[i].Y + dy < 0) || (p.Dobozok[i].X + dx > p.Falak.GetLength(1)) || (p.Dobozok[i].X + dx < 0))))
                    {
                        p.Dobozok[i] = new Point(p.Dobozok[i].X + dx, p.Dobozok[i].Y + dy);
                        p.Dobozok = p.Dobozok;

                        int cntHelyenDoboz = 0;
                        ///lerakón lévő dobozok száma
                        for (int j = 0; j < p.Dobozok.Length; j++)
                        {
                            for (int k = 0; k < p.Dobozok.Length; k++)
                            {
                                if (p.Dobozok[j].X == p.Lerakok[k].X && p.Dobozok[j].Y == p.Lerakok[k].Y)
                                {
                                    cntHelyenDoboz++;
                                }
                            }
                        }

                        if (cntHelyenDoboz == p.Dobozok.Length)
                        {
                            if (CelbaErt != null)
                                CelbaErt(null,EventArgs.Empty);
                            
                        }

                    }
                }
            }

            p.Jatekos = new Point(p.Jatekos.X + dx, p.Jatekos.Y + dy);
        }




    }
}
