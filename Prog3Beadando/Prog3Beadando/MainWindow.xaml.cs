﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Prog3Beadando
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 


    public partial class MainWindow : Window
    {
        ViewModel vm = new ViewModel();
        
        public MainWindow()
        {
            InitializeComponent();            
                               
            DataContext = vm;
        }

        private void btnNewGame_Click(object sender, RoutedEventArgs e)
        {
            int selected = cmbMaps.SelectedIndex;

            if (cmbMaps.SelectedItem !=null)
            {

                Palya tmp = PalyaManager.Beolvas(cmbMaps.SelectedIndex + 1);  
                            
                                
                GameWindow gw = new GameWindow(tmp, vm.Plyr);
                //constructorba paraméterként a játékos és cmbMaps Selected Item-ja.   

                gw.ShowDialog();
                if(gw.DialogResult == true)
                {
                   vm.Palyak[selected].Eredmenyek.Add(new Eredmeny() { Nev = vm.Plyr.Nev, LepesSzam = vm.Plyr.Pontszam });
                }
                EredmenyManager.EredmenyKiir(vm.Palyak);
            }
            else
            {
                MessageBox.Show("Előszőr válassz ki egy pályát!","Hiba",MessageBoxButton.OK,MessageBoxImage.Warning);
            }           
            
        }


        private void btnQuit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void btnChangePlayer_Click(object sender, RoutedEventArgs e)
        {
            InputBox.Visibility = System.Windows.Visibility.Visible;
        }       

        private void NoButton_Click(object sender, RoutedEventArgs e)
        {
            InputBox.Visibility = System.Windows.Visibility.Collapsed;            
            InputTextBox.Text = String.Empty;
        }

        private void YesButton_Click(object sender, RoutedEventArgs e)
        {            
            InputBox.Visibility = System.Windows.Visibility.Collapsed;
            String ujnev = InputTextBox.Text;
            vm.Plyr = new Jatekos(ujnev);                
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            EredmenyWindow ew = new EredmenyWindow(vm);
            ew.ShowDialog();
        }
    }
}
